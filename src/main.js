import Vue from 'vue'
import App from './App.vue'
import VueResource from 'vue-resource'
import VModal from 'vue-js-modal'

Vue.use(VModal)
Vue.use(VueResource);

new Vue({
  el: '#app',
  render: h => h(App)
})
